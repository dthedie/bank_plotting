import PySimpleGUI as sg
import matplotlib.pyplot as plt
from os.path import isdir
from Bank_plotting import Statement
from Bank_plotting.layout import make_window
from matplotlib.backends.backend_tkagg import FigureCanvasTkAgg
from PIL import ImageGrab

fig, ax = plt.subplots(dpi=100)


def draw_figure(canvas, figure):
    if not hasattr(draw_figure, 'canvas_packed'):
        draw_figure.canvas_packed = {}
    figure_canvas_agg = FigureCanvasTkAgg(figure, canvas)
    figure_canvas_agg.draw()
    widget = figure_canvas_agg.get_tk_widget()
    if widget not in draw_figure.canvas_packed:
        draw_figure.canvas_packed[widget] = figure
        widget.pack(side='top', fill='both', expand=1)
    return figure_canvas_agg


def save_element_as_file(element, filename):
    widget = element.Widget
    box = (widget.winfo_rootx(),
           widget.winfo_rooty(),
           widget.winfo_rootx() + widget.winfo_width(),
           widget.winfo_rooty() + widget.winfo_height()
           )
    grab = ImageGrab.grab(bbox=box)

    grab.save(filename)


# Create the Window
window = make_window()
# Event Loop to process "events" and get the "values" of the inputs
while True:
    event, values = window.read()
    if event == sg.WIN_CLOSED or event == 'Exit':  # if user closes window or clicks Exit
        break
    if event == 'Reset plot':
        ax.cla()
        try:
            fig_agg.get_tk_widget().forget()
        except:
            pass
        fig_agg = draw_figure(window['canvas'].TKCanvas, fig)
        window['console'].Update('Plot reset')
    if event == 'Plot' and not isdir(values['folder']):
        window['console'].Update('Folder not found')
    if event == 'Plot' and isdir(values['folder']):
        window['console'].Update('Plotting...')
        try:
            fig_agg.get_tk_widget().forget()
        except:
            pass
        statement = Statement(folder=values['folder'], bank=values['Bank'])
        statement.combine_files()
        statement.formatting()
        statement.filter_dates(values['lower_date'], values['upper_date'])
        statement.replace_keywords(values['replace'], case=values['case'])
        if statement.data.Description.str.contains(values['keyword'], case=values['case']).any():
            statement.filter_keyword(values['keyword'], case=values['case'])
            statement.plot(plot_type=values['PlotType'], freq=frequencies[values['freq']], axis=ax)
            fig.autofmt_xdate()
        else:
            window['console'].Update('Keyword not found')
        fig_agg = draw_figure(window['canvas'].TKCanvas, fig)
    if event == 'Save plot':
        filename = sg.popup_get_file('Save plot as...', save_as=True)
        save_element_as_file(window['canvas'], filename)

window.close()
