import PySimpleGUI as sg


frequencies = {'Year': 'Y', 'Quarter': 'Q', 'Month': 'ME',
               'Week': 'W', 'Day': 'D'}


def make_window(theme=None):

    NAME_SIZE = 50

    def name(name):
        dots = NAME_SIZE-len(name)-2
        return sg.Text(name + ' ' + ' '*dots, size=(NAME_SIZE, 1), justification='r', pad=(0, 0), font='Arial 10')

    bank_list = ['TSB']
    plot_types = ['Debit', 'Credit', 'Balance', 'Top expenses']

    sg.theme('DarkAmber')

    layout_input = [[name('Input folder (contains .csv statements)'),
                     sg.InputText(size=(32, 22),
                                  k='folder'),
                     sg.FolderBrowse()
                     ],
                    [name('Bank'),
                     sg.Combo(bank_list,
                              default_value='TSB',
                              s=(15, 22),
                              enable_events=True,
                              readonly=False,
                              k='Bank')
                     ]]

    layout_label = [[name('Replace keyword (Old1:New1, Old2:New2,...)'),
                     sg.Input(size=(32, 22),
                              k='replace')
                     ]]

    layout_plot = [[name('Operation type'),
                    sg.Combo(plot_types,
                             default_value='Debit',
                             s=(15, 22),
                             enable_events=True,
                             readonly=False,
                             k='PlotType')
                    ],
                   [name('Restrict dates (YYYY-MM-DD)'),
                    sg.InputText(size=(10, 22),
                                 k='lower_date'),
                    sg.Text('to'),
                    sg.InputText(size=(10, 22),
                                 k='upper_date')
                    ],
                   [name('Frequency'),
                    sg.Combo(list(frequencies.keys()),
                             default_value='Month',
                             s=(15, 22),
                             enable_events=True,
                             readonly=False,
                             k='freq')
                    ],
                   [name('Search keyword'),
                    sg.InputText(size=(17, 22),
                                 k='keyword'),
                    sg.Checkbox('Case sensitive',
                                default=False,
                                k='case')
                    ],
                   [sg.Button('Plot'),
                    sg.Button('Reset plot'),
                    sg.Button('Save plot'),
                    sg.Button('Exit')
                    ],
                   [sg.Text('')],
                   [sg.Frame('Console',
                             [[sg.Text('',
                                       size=(95, 10),
                                       k='console')
                               ]]
                             )
                    ]]

    layout_l = [[sg.TabGroup([[sg.Tab('Input', layout_input),
                               sg.Tab('Labels', layout_label),
                               sg.Tab('Plot', layout_plot)
                               ]]
                             )
                 ]]

    layout_r = [[sg.Canvas(size=(600, 400), k='canvas')]]

    layout = [[sg.T('Bank plotting', font='_ 18', justification='c', expand_x=True)],
              [sg.Col(layout_l), sg.Col(layout_r)]]

    window = sg.Window('Bank plotting', layout, finalize=True)

    return window
