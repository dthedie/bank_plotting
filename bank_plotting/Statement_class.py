from os import listdir
from os.path import isdir, join
import pandas as pd
import seaborn as sns

from .Statement_formatting import TSB_formatting
from .Util import is_valid_date


class Statement():
    def __init__(self, folder='', bank=''):
        self.folder = folder
        self.bank = bank
        self.keyword = ''

    def __str__(self):
        return 'Statement from ' + self.bank

    def combine_files(self):
        files = [fname for fname in listdir(self.folder) if not isdir(fname)]
        statement = pd.DataFrame(columns=['Transaction date',
                                          'Transaction Type',
                                          'Transaction description',
                                          'Debit Amount',
                                          'Credit amount',
                                          'Balance'])
        for f in files:
            statement_part = pd.read_csv(join(self.folder, f))
            statement = pd.concat([statement, statement_part], join='inner', axis=0)
        self.data = statement

    def formatting(self):
        if self.bank == 'TSB':
            self.data = TSB_formatting(self.data)

    def filter_keyword(self, keyword, case: bool = False):
        self.keyword = keyword
        if not keyword == '':
            self.data = (self.data
                             .loc[self.data.Description.str.contains(keyword, case=case)])

    def filter_dates(self, lower, upper):
        if is_valid_date(lower, date_format='%Y-%m-%d'):
            self.data = (self.data
                             .loc[self.data.Date > lower]
                         )
        if is_valid_date(upper, date_format='%Y-%m-%d'):
            self.data = (self.data
                             .loc[self.data.Date < upper]
                         )

    def replace_keywords(self, replace='', case: bool = False):
        if not replace == '':
            df = self.data
            replace = replace.split(',')
            for item in replace:
                old, new = item.split(':')
                old = old.strip()
                new = new.strip()
                df = (df.assign(Description=lambda df: df.Description.str.replace(old, new, case=case, regex=False)))
            self.data = df

    def plot(self, plot_type: str = 'Debit', freq: str = 'M', axis=None):
        if plot_type == 'Debit' or plot_type == 'Credit':
            df = (self.data
                  .groupby(pd.Grouper(key='Date', freq=freq))
                  .agg({plot_type: 'sum'})
                  )
            sns.lineplot(data=df, x='Date', y=plot_type, ax=axis)
            # axis.set_xticklabels(rotation=45)
        elif plot_type == 'Balance':
            df = (self.data
                  .groupby(pd.Grouper(key='Date', freq=freq))
                  .agg({plot_type: 'mean'})  # This is a silly idea, doesn't work
                  )
            sns.lineplot(data=df, x='Date', y=plot_type, ax=axis)
        elif plot_type == 'Top expenses':
            (self.data
                 .groupby(by=[pd.Grouper(key='Date', freq=freq), 'Description'])
                 .sum()
                 .sort_values(by='Debit', ascending=False)
                 .reset_index()
                 .drop_duplicates(subset='Description')
                 .head(10)
                 .plot.barh(x='Description', y='Debit', xlabel='', ylabel='Amount (£)', ax=axis)
             )
