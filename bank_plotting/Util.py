from datetime import datetime


def is_valid_date(date: str = '', date_format: str = '%Y-%m-%d'):
    res = True
    try:
        res = bool(datetime.strptime(date, date_format))
    except ValueError:
        res = False
    return res
