import pandas as pd


def TSB_formatting(df):
    return (df
            .drop_duplicates()
            .rename(columns={'Transaction date': 'Date',
                             'Transaction Type': 'Type',
                             'Transaction description': 'Description',
                             'Debit Amount': 'Debit',
                             'Credit amount': 'Credit'})
            .assign(Date=lambda df: pd.to_datetime(df.Date),
                    Type=lambda df: df.Type.astype('category'),
                    )
            )
