import setuptools

with open("Readme.md", "r") as fh:
    long_description = fh.read()

setuptools.setup(
    name="Bank_plotting",
    version="0.0.11",
    author="Daniel Thedie",
    author_email="dthedie@gmail.com",
    description="Plot bank account debit, credit and balance from downloaded statements",
    long_description=long_description,
    long_description_content_type="text/markdown",
    url="https://gitlab.com/dthedie/Bank_plotting",
    packages=setuptools.find_packages(),
    python_requires='>=3',
    install_requires=['pandas', 'matplotlib', 'PySimpleGUI']
)
