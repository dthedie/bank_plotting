# Bank plotting

This package can be used to plot specific expenses, credit or account balance from downloaded bank statements in csv format.

Install the package in Python with the command: `!pip git+https://gitlab.com/dthedie/bank_plotting.git`

In a terminal, run the main script: `$ python Bank_plotting_gui.py`

Currently supported banks:
- TSB

If your bank is not in the list, please raise an issue, and attach a sample statement in csv format (or equivalent).